import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Main {
    public static void main(String[] args) {
        MissionControlSoftware missionControl = new MissionControlSoftware();

        // Create and add satellites
        Satellite satellite1 = new Satellite("Satellite 1");
        Satellite satellite2 = new Satellite("Satellite 2");
        missionControl.addSatellite(satellite1);
        missionControl.addSatellite(satellite2);

        // Activate satellite
        missionControl.activateSatellite("Satellite 1");

        // Deactivate satellite
        missionControl.deactivateSatellite("Satellite 2");

        // Display satellite status
        missionControl.displaySatelliteStatus();

        // Start the controlled system listener
        startControlledSystemListener(missionControl);
    }

    private static void startControlledSystemListener(MissionControlSoftware missionControl) {
        new Thread(() -> {
            try (ServerSocket serverSocket = new ServerSocket(9000)) {
                System.out.println("Controlled System Listener started. Waiting for connections...");
                while (true) {
                    Socket clientSocket = serverSocket.accept();
                    new Thread(new ControlledSystemHandler(clientSocket, missionControl)).start();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }
}


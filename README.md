


A  Satellite class that represents a satellite with its name and an isActive flag to indicate whether it is active or not. 

A ControlledSystemHandler class that implements the Runnable interface. It listens for connections from controlled systems and handles the incoming requests. The MissionControlSoftware class  has a sendCommandToControlledSystem method that establishes a connection with the controlled system and sends activation or deactivation commands.

Additionally, a startControlledSystemListener method is added to the Main class. It creates a new thread to listen for connections from controlled systems using a ServerSocket. When a connection is accepted, a ControlledSystemHandler instance is created to handle the incoming requests.

 Java's Socket and ServerSocket classes to establish a TCP/IP socket connection between the mission control software and controlled systems.

Navigate to the directory where you saved the Java files and compile both java files with the commands below:

>> javac MissionControlSoftware.java
>> javac Main.java

This will generate the bytecode files MissionControlSoftware.class and Main.class.

After that,run the program with the command below:

>> java Main

The program will start running, and you should see the output in the command prompt or terminal.


Also included Junit Testing. 

Download the JUnit 4 Jar files from https://jar-download.com/artifacts/junit/junit/4.12/source-code into the same directory as the source codes and unzip it. It should contains two files junit-4.12.jar and junit-4.12.jar:hamcrest-core-1.3.jar.

Use the command below to unzip:

>> unzip jar_files.zip


To run the test, follow the commands below:


>> javac -cp .:junit-4.12.jar MissionControlSoftwareTest.java

>> java -cp .:junit-4.12.jar:hamcrest-core-1.3.jar org.junit.runner.JUnitCore MissionControlSoftwareTest


                 OR

Another method is proceeding to compile and run the code with JUnit tests. 

>> javac -cp .:junit-4.12.jar MissionControlSoftware.java Main.java

>> java -cp .:junit-4.12.jar  Main


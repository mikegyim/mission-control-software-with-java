import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

class Satellite {
    private String name;
    private boolean isActive;

    public Satellite(String name) {
        this.name = name;
        this.isActive = false;
    }

    public String getName() {
        return name;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}

class MissionControlSoftware {
    private List<Satellite> satellites;

    public MissionControlSoftware() {
        satellites = new ArrayList<>();
    }

    public void addSatellite(Satellite satellite) {
        satellites.add(satellite);
    }

    public List<Satellite> getSatellites() {
        return satellites;
    }

    public void activateSatellite(String name) {
        for (Satellite satellite : satellites) {
            if (satellite.getName().equals(name)) {
                satellite.setActive(true);
                System.out.println("Satellite " + name + " activated.");
                // Send activation command to the controlled system
                sendCommandToControlledSystem(name, "ACTIVATE");
                return;
            }
        }
        System.out.println("Satellite " + name + " not found.");
    }

    public void deactivateSatellite(String name) {
        for (Satellite satellite : satellites) {
            if (satellite.getName().equals(name)) {
                satellite.setActive(false);
                System.out.println("Satellite " + name + " deactivated.");
                // Send deactivation command to the controlled system
                sendCommandToControlledSystem(name, "DEACTIVATE");
                return;
            }
        }
        System.out.println("Satellite " + name + " not found.");
    }

    public void displaySatelliteStatus() {
        for (Satellite satellite : satellites) {
            System.out.println("Satellite: " + satellite.getName() + ", Active: " + satellite.isActive());
        }
    }

    private void sendCommandToControlledSystem(String satelliteName, String command) {
        try (Socket socket = new Socket("127.0.0.1", 9000)) {
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            out.println(satelliteName + "," + command);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

class ControlledSystemHandler implements Runnable {
    private Socket clientSocket;
    private MissionControlSoftware missionControl;

    public ControlledSystemHandler(Socket clientSocket, MissionControlSoftware missionControl) {
        this.clientSocket = clientSocket;
        this.missionControl = missionControl;
    }

    @Override
    public void run() {
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            String receivedData = in.readLine();
            if (receivedData != null) {
                String[] parts = receivedData.split(",");
                if (parts.length == 2) {
                    String satelliteName = parts[0];
                    String command = parts[1];
                    if (command.equals("STATUS")) {
                        // Send satellite status to the controlled system
                        sendSatelliteStatusToControlledSystem(satelliteName);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendSatelliteStatusToControlledSystem(String satelliteName) {
        for (Satellite satellite : missionControl.getSatellites()) {
            if (satellite.getName().equals(satelliteName)) {
                try (Socket socket = new Socket("127.0.0.1", 9000)) {
                    PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                    out.println(satelliteName + "," + satellite.isActive());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return;
            }
        }
    }
}


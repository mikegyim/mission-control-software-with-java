import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class MissionControlSoftwareTest {
    private MissionControlSoftware missionControl;

    @Before
    public void setUp() {
        missionControl = new MissionControlSoftware();

        // Create and add satellites
        Satellite satellite1 = new Satellite("Satellite 1");
        Satellite satellite2 = new Satellite("Satellite 2");
        missionControl.addSatellite(satellite1);
        missionControl.addSatellite(satellite2);
    }

    @Test
    public void testActivateAndDeactivateSatellite() {
        missionControl.activateSatellite("Satellite 1");
        missionControl.deactivateSatellite("Satellite 2");

        List<Satellite> satellites = missionControl.getSatellites();
        Assert.assertTrue(satellites.get(0).isActive());
        Assert.assertFalse(satellites.get(1).isActive());
    }
}

